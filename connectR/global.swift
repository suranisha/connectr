//
//  global.swift
//  connectR
//
//  Created by sha on 10/9/16.
//  Copyright © 2016 sha. All rights reserved.
//

import Foundation

struct physicsBodyCategory{
    let box:UInt32 = 1
    let disc:UInt32 = 2
}

struct storyBoardVar {
    public static var row:Int = 0
    public static var column:Int=0
    public static var r:Int=0
    
    
    public static var first=false
    public static var depth=0
    public static var humanColor=0
    
    public static var play = false
    public static var turn = -1
    
    public static var undo = false
}

struct gameArray{
    public static var game:connectR!
}

extension Collection {
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Iterator.Element? {
        return index >= startIndex && index < endIndex ? self[index] : nil
    }
}

/* 
 m = row
 n = column
 --[column[row]]
 r = match
 first = (false:computers first turn ) (true: human's first turn )
 depth = depth * 2 tree allowed
 maxTime = in seconds
 human color = ( 0: red    1: black)
 
 play =1: start game
 turn = 1: human 0: computer
 
 
 
 */
