


//=========================================
//
//  AlphaBeta.swift
//  connectR
//
//  Created by sha on 10/11/16.
//  Copyright © 2016 sha. All rights reserved.
//

import Foundation

class AlphaBeta{
    
    let gameData:[[Int]]!
    let size:[Int]!
    var totalPoints = 0
    var globalNode:Node!
    var depth:Int!
    let minmax:Int
    var column:Int!
    var done = false
    
    init(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, node:Node = Node(), minmax:Int=1, column:Int = 0){
        self.gameData = data
        self.size = size
        self.globalNode = node
        self.depth = depth
        self.minmax = minmax
        self.column = column
    }
    
    func start() -> Node{
        if(minmax==1){
            if(globalNode.getBeta()<globalNode.getAlpha()){
                return globalNode
            }
            alpha()
        }else{
            if(globalNode.getAlpha()>globalNode.getBeta()){
                return globalNode
            }
            beta()
        }
        
        if(self.depth==storyBoardVar.depth && self.minmax==1 && self.globalNode.getColumn()<0){
            print("You won!!")
            self.globalNode.setColumn(column: -1)
        }
        if(self.depth==storyBoardVar.depth && self.minmax==1 && self.globalNode.getWin()){
            print("I Won")
        }
        return self.globalNode
    }
    
    
    
    func alpha(){
        //depth
        if(self.depth == 0){
            let win = self.didSomebodyWin(inColumn: self.column, inRow: (self.size?[self.column])!-1, data:self.gameData!)
            self.globalNode.setValue(value: -(win.maxPoints * win.maxPoints))
            self.globalNode.setColumn(column: self.column)
            return
            
        }
        var didWinAtLeastOnce = false;
        for i in 0...storyBoardVar.column-1{
            if(self.isColumnFull(column: i, size:self.size)){ continue }
            
            var newData = self.gameData
            var newSize = self.size
            
            newData?[self.size[i]][i] = 0
            newSize?[i] += 1
            
            let win = self.didSomebodyWin(inColumn: i, inRow: (newSize?[i])!-1, data:newData!)
            
            if(win.win && didWinAtLeastOnce){
                
                self.globalNode.setAlpha(alpha: (self.globalNode.getValue()*2))
                self.globalNode.setValue(value: self.globalNode.getAlpha())
                self.globalNode.setWin(win: true)
                continue
            }
            
            if(win.win){
                self.globalNode.setColumn(column: i)
                self.globalNode.setAlpha(alpha: (win.maxPoints*win.maxPoints))
                self.globalNode.setValue(value: self.globalNode.getAlpha())
                self.globalNode.setWin(win: true)
                didWinAtLeastOnce = true
                continue
            }
            
            if(didWinAtLeastOnce){
                continue
            }
            
            if(self.isTie(size: self.size)){continue}
            
            let minNode = AlphaBetaWithThread(data: newData!, size: newSize!, depth: self.depth, node:self.globalNode, minmax:0, column:i).start()
            if((minNode.getValue()) > self.globalNode.getAlpha()){
                self.globalNode.setAlpha(alpha: minNode.getValue())
                self.globalNode.setColumn(column: i)
                self.globalNode.setValue(value: self.globalNode.getAlpha())
            }
        }
    }
    
    //min
    func beta(){
        var didWinAtLeastOnce = false;
        for i in 0...storyBoardVar.column-1{
            //return if column is full
            if(isColumnFull(column: i, size:size)){ continue }
            
            var newData = self.gameData
            var newSize = size
            newData?[size[i]][i] = 1
            newSize?[i] += 1
            
            let win = didSomebodyWin(inColumn: i, inRow: (newSize?[i])!-1, data: newData!)
            
            if(win.win && didWinAtLeastOnce){
                globalNode.setBeta(beta: (globalNode.getValue()*2))
                globalNode.setValue(value: globalNode.getBeta())
                globalNode.setWin(win: true)
                continue
            }
            
            if(win.win){
                globalNode.setColumn(column: i)
                globalNode.setBeta(beta: -(win.maxPoints*win.maxPoints))
                globalNode.setValue(value: globalNode.getBeta())
                globalNode.setWin(win: true)
                didWinAtLeastOnce = true
                continue
            }
            
            if(didWinAtLeastOnce){
                continue
            }
            
            if(isTie(size: size)){continue}
            
            let maxNode = AlphaBetaWithThread(data: newData!, size: newSize!, depth: depth-1, node:globalNode, minmax: 1, column:i).start()
            //if depth == 1 then this is leaf
            if(depth == 1){
                if(maxNode.getValue() < globalNode.getBeta()){
                    globalNode.setBeta(beta: maxNode.getValue())
                    globalNode.setValue(value: globalNode.getBeta())
                    globalNode.setColumn(column: maxNode.getColumn())
                }
                continue
            }
            
            if(maxNode.getValue() < globalNode.getBeta()){
                globalNode.setBeta(beta: maxNode.getValue())
                globalNode.setColumn(column: maxNode.getColumn())
                globalNode.setValue(value: globalNode.getBeta())
            }
            
        }
        
        
    }
    
    
    
    
    
    
    //============================= helper functions =============================
    
    func points() -> (humanMaxPoints:Int, computerMaxPoints:Int){
        var playerMaxPoints = 0
        var computerMaxPoints = 0
        
        for i in 0...self.gameData.count-1 {
            if(size[i] == 0){ continue }
            for j in 0...size[i]-1 {
                let win = didSomebodyWin(inColumn: i, inRow: j, data:gameData)
                if(win.player==0){
                    if(computerMaxPoints<win.maxPoints){
                        computerMaxPoints =  win.maxPoints
                    }
                }else if(win.player==1){
                    if(playerMaxPoints < win.maxPoints){
                        playerMaxPoints = win.maxPoints
                    }
                }
            }
        }
        
        
        return (playerMaxPoints, computerMaxPoints)
    }
    
    
    
    func isColumnFull(column:Int, size:[Int]) -> Bool{
        return size[column] == storyBoardVar.row
    }
    
    func didSomebodyWin(inColumn:Int, inRow:Int, data:[[Int]]) -> (win:Bool, player:Int, maxPoints:Int){
        let discs = numberOfDiscInRow(inColumn: inColumn, inRow: inRow, data:data)
        if(discs.numberOfDisc == storyBoardVar.r){
            return (true, discs.player, discs.numberOfDisc)
        }
        return (false, discs.player, discs.numberOfDisc) //-1 nobody win
    }
    
    func isTie(size:[Int]) -> Bool {
        var full:Bool = true
        for i in 0...storyBoardVar.row-1{
            if(size[i] != storyBoardVar.column){
                full = false
                break;
            }
        }
        return full
    }
    
    func numberOfDiscInRow(inColumn:Int, inRow:Int, data:[[Int]]) -> (player:Int, numberOfDisc:Int){
        if(data[inRow][inColumn] == -1){
            return (-1, 0)
        }
        let player = data[inRow][inColumn]
        let r = storyBoardVar.r
        
        var posibleOnTopRight = true
        var posibleOnDownLeft = true
        var numberOfDLDisc=1;
        
        var posibleOnRight = true;
        var posibleOnLeft = true
        var numberOfRLDisc=1;
        
        var posibleOnDownRight = true;
        var posibleOnTopLeft = true
        var numberOfDRDisc=1;
        
        var posibleOnDown = true;
        var numberOfDDisc=1;
        
        //vertical
        for i in 1...storyBoardVar.r-1{
            // down
            if(posibleOnDown && checkBound(column: inColumn, row: inRow-i)){
                if(data[inRow-i][inColumn] == player){
                    numberOfDDisc+=1
                }else{
                    posibleOnDown = false;
                }
            }
            
            if(numberOfDDisc==r){
                return (player,numberOfDDisc)
            }
            
            if(!posibleOnDown){
                break
            }
        }
        
        //horizontal
        for i in 1...storyBoardVar.r-1{
            //right
            if(posibleOnRight && checkBound(column: inColumn+i, row: inRow)){
                if(data[inRow][inColumn+i] == player){
                    numberOfRLDisc+=1
                }else{
                    posibleOnRight = false;
                }
            }
            //left
            if(posibleOnLeft && checkBound(column: inColumn-i, row: inRow)){
                if(data[inRow][inColumn-i] == player){
                    numberOfRLDisc+=1
                }else{
                    posibleOnLeft = false;
                }
            }
            if(numberOfRLDisc==r){
                return (player,numberOfRLDisc)
            }
            
            if(!posibleOnLeft && !posibleOnRight){
                break
            }
        }
        
        //front slash
        for i in 1...storyBoardVar.r-1{
            // top right
            if(posibleOnTopRight && checkBound(column: inColumn+i, row: inRow+i)){
                if(data[inRow+i][inColumn+i] == player){
                    numberOfDLDisc+=1
                }else{
                    posibleOnTopRight = false;
                }
            }
            // bottom left
            if(posibleOnDownLeft && checkBound(column: inColumn-i, row: inRow-i)){
                if(data[inRow-i][inColumn-i] == player){
                    numberOfDLDisc+=1
                }else{
                    posibleOnDownLeft = false;
                }
            }
            
            if(numberOfDLDisc==r){
                return (player,numberOfDLDisc)
            }
            
            if(!posibleOnDownLeft && !posibleOnTopRight){
                break
            }
        }
        
        //back slash
        for i in 1...storyBoardVar.r-1{
            // bottom right
            if(posibleOnDownRight && checkBound(column: inColumn+i, row: inRow-i)){
                if(data[inRow-i][inColumn+i] == player){
                    numberOfDRDisc+=1
                }else{
                    posibleOnDownRight = false;
                }
            }
            // top left
            if(posibleOnTopLeft && checkBound(column: inColumn-i, row: inRow+i)){
                if(data[inRow+i][inColumn-i] == player){
                    numberOfDRDisc+=1
                }else{
                    posibleOnTopLeft = false;
                }
            }
            
            if(numberOfDRDisc==r){
                return (player,numberOfDRDisc)
            }
            
            if(!posibleOnTopLeft && !posibleOnDownRight){
                break
            }
        }
        let all:[Int] = [numberOfRLDisc, numberOfDRDisc, numberOfDDisc, numberOfDLDisc]
        return (player, getMax(myArray: all))
    }
    
    func checkBound(column:Int, row:Int) -> Bool{
        if(column<0 || row<0 || column>=storyBoardVar.column || row>=storyBoardVar.row){
            return false
        }
        return true
    }
    
    func getMax(myArray:[Int])-> Int{
        var max = -storyBoardVar.r
        for i in 0...myArray.count-1{
            if(myArray[i]>max){
                max = myArray[i]
            }
        }
        return max
    }
    
    func getMin(myArray:[Int]) -> Int {
        var min = storyBoardVar.r
        for i in 0...myArray.count-1{
            if(myArray[i]<min){
                min = -myArray[i]
            }
        }
        return min
    }
    
    func getEmptyColumn() -> Int {
        var possibleColumn:[Int] = []
        for i in 0...self.size.count-1{
            if(!isColumnFull(column: i, size: self.size)){
                possibleColumn.append(i)
            }
        }
        if (possibleColumn.count>0){
            return possibleColumn[Int( arc4random_uniform(UInt32((possibleColumn.count))))]
        }else{
            return -1
        }
    }
}
