//
//  GameScene.swift
//  connectR
//
//  Created by sha on 10/7/16.
//  Copyright © 2016 sha. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var connectRGame:connectR?
    
    override func didMove(to view: SKView) {
        
    }
    
    func touchDown(atPoint pos : CGPoint) {
       
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func mouseDown(with event: NSEvent) {
        let location = event.location(in: self)
        let node = nodes(at: location)[0] as! SKSpriteNode
        if((node.name) != nil){
            if(storyBoardVar.turn==1){
                self.addChild( (connectRGame?.humanPlay(node: node))! )
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                    self.addChild( (self.connectRGame?.computerPlay(topMenu: self.childNode(withName: "background")?.childNode(withName: "mainBackground")?.childNode(withName: "topMenu") as! SKSpriteNode))! )
                })
                //make computers turn
                
            }else{
                self.addChild( (connectRGame?.computerPlay(topMenu: self.childNode(withName: "background")?.childNode(withName: "mainBackground")?.childNode(withName: "topMenu") as! SKSpriteNode))! )
                storyBoardVar.turn = 1
            }
        }
    }
    
    override func mouseDragged(with event: NSEvent) {
        
    }
    
    override func mouseUp(with event: NSEvent) {
        self.touchUp(atPoint: event.location(in: self))
    }
    
    override func keyDown(with event: NSEvent) {
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        if(storyBoardVar.play){
            if (connectRGame == nil) {
                connectRGame = connectR(Int: storyBoardVar.column, r: storyBoardVar.row)
                print( "connect r game created" )
                if(storyBoardVar.first==false){
                    //make computers turn
                    self.addChild( (connectRGame?.computerPlay(topMenu: self.childNode(withName: "background")?.childNode(withName: "mainBackground")?.childNode(withName: "topMenu") as! SKSpriteNode))! )
                    storyBoardVar.turn = 1
                }
            }
            
            if(storyBoardVar.undo){
                let columns = connectRGame?.undo()
                if columns != nil{
                    removeBall(name: (columns)!)
                }
                storyBoardVar.undo = false
                
            }
        }
    }
    
    func removeBall(name:String){
        for child in self.children as! [SKSpriteNode] {
            if(child.name==name){
                self.removeChildren(in: [child])
                break
            }
        }
    }
    
    
}
