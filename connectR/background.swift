//
//  background.swift
//  connectR
//
//  Created by sha on 10/7/16.
//  Copyright © 2016 sha. All rights reserved.
//

import SpriteKit;
import GameplayKit;
import AppKit;

class Background: SKSpriteNode {
   
    var screenSize:NSRect?
    let column=storyBoardVar.column;
    let row=storyBoardVar.row;
    var mainBackground:SKSpriteNode!;
    var topMenu:SKSpriteNode = SKSpriteNode();
    
    func createbg(screenSize:NSRect) {
        self.screenSize=screenSize
        self.anchorPoint=CGPoint(x: 0, y: 0)
        self.zPosition = 200
        self.name = "background"
        
        mainBackground = SKSpriteNode(color: NSColor.blue, size: CGSize(width: ((screenSize.width)), height: ((screenSize.height))-200))
        mainBackground.position.y -= 20 ;
        mainBackground.name = "mainBackground"
        self.addChild(mainBackground)
        
        topMenu.name = "topMenu"
        mainBackground.addChild(topMenu)
        
        bg()
    }
    
    func bg(){
        var box:SKSpriteNode!
        
        
        for i in 0...column-1{
            for j in 0...row-1{
                box = SKSpriteNode(imageNamed: "mainBox")
                
                box.size.width = (mainBackground.size.width)/CGFloat(column)
                box.size.height = (mainBackground.size.height)/CGFloat((row+1))
                box.anchorPoint=CGPoint(x: 0, y: 0)
                
                box.position.x = (CGFloat(i) * box.size.width);
                box.position.x -= (((screenSize?.width)!)/2)
            
                box.position.y = (CGFloat(j) * box.size.height)-((mainBackground.size.height)/2)
                
                let leftPhysicsNode = SKNode()
                leftPhysicsNode.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 2, height: box.size.height))
                leftPhysicsNode.position = box.position
                leftPhysicsNode.position.y+=40
                leftPhysicsNode.physicsBody?.affectedByGravity = false
                leftPhysicsNode.physicsBody?.isDynamic = false
                leftPhysicsNode.physicsBody?.categoryBitMask = 1
                leftPhysicsNode.physicsBody?.collisionBitMask = 2
                mainBackground.addChild(leftPhysicsNode)
                
                if(j==0){
                    let botPhysicsNode = SKNode()
                    botPhysicsNode.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: box.size.width, height: 2))
                    botPhysicsNode.position = box.position
                    botPhysicsNode.position.y+=0
                    botPhysicsNode.physicsBody?.affectedByGravity = false
                    botPhysicsNode.physicsBody?.isDynamic = false
                    botPhysicsNode.physicsBody?.categoryBitMask = 1
                    botPhysicsNode.physicsBody?.collisionBitMask = 2
                    mainBackground.addChild(botPhysicsNode)
                }
                
                if(i==column-1){
                    let rightPhysicsNode = SKNode()
                    rightPhysicsNode.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 2, height: box.size.height))
                    rightPhysicsNode.position = box.position
                    rightPhysicsNode.position.x+=box.size.width
                    rightPhysicsNode.physicsBody?.affectedByGravity = false
                    rightPhysicsNode.physicsBody?.isDynamic = false
                    rightPhysicsNode.physicsBody?.categoryBitMask = 1
                    rightPhysicsNode.physicsBody?.collisionBitMask = 2
                    mainBackground.addChild(rightPhysicsNode)
                }
                
                mainBackground.addChild(box)
                
                if(j==row-1){
                    createTopMenu(i:i, j:j+1)
                }
            }
        }
    }
    
    func createTopMenu(i:Int, j:Int){
        
        let box = SKSpriteNode(imageNamed: "whiteBox")
        
        
        box.size.width = (mainBackground.size.width)/CGFloat(column)
        box.size.height = (mainBackground.size.height)/CGFloat(row+1)
        
        
        box.anchorPoint=CGPoint(x: 0, y: 0)
        
        box.name = "\(i)"
        
        box.position.x = (CGFloat(i) * box.size.width);
        box.position.x -= (((screenSize?.width)!)/2)
        
        box.position.y = (CGFloat(j) * box.size.height)-((mainBackground.size.height)/2)
        
        box.xScale*=0.99
        
        topMenu.addChild(box)
    }
    

}
