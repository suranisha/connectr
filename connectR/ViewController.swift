//
//  ViewController.swift
//  connectR
//
//  Created by sha on 10/7/16.
//  Copyright © 2016 sha. All rights reserved.
//

import Cocoa
import SpriteKit
import GameplayKit

class ViewController: NSViewController {
    
    var human = 0
    var color = 0
    

    @IBOutlet var skView: SKView!
    
    
    @IBOutlet weak var allowedDepth: NSTextField!
    
    @IBOutlet weak var column: NSTextField!
    
    @IBOutlet weak var row: NSTextField!
    
    @IBOutlet weak var r: NSTextField!
    
    
    var scene:SKScene!;
    override func viewDidLoad() {
        super.viewDidLoad()

        if let view = self.skView {
            // Load the SKScene from 'GameScene.sks'
            scene = SKScene(fileNamed: "GameScene")
            // Set the scale mode to scale to fit the window
            scene.scaleMode = .aspectFill
            scene.anchorPoint=CGPoint(x: 0.5, y: 0.5)
            // Present the scene
            view.presentScene(scene)
            
//            view.showsPhysics = true//remove this
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }
    
    
    @IBAction func redBlueSelector(_ sender: NSButton) {
        self.color = sender.tag
    }
    
    @IBAction func humanOrComputer(_ sender: AnyObject) {
        self.human = sender.tag
    }
    
    @IBAction func start(_ sender: AnyObject) {
        storyBoardVar.column = Int(self.column.intValue)
        storyBoardVar.row = Int(self.row.intValue)
        storyBoardVar.r = Int(self.r.intValue)
        
        storyBoardVar.depth = Int(self.allowedDepth.intValue)
        
        if(human==0){
            storyBoardVar.first = false;
        }else{
            storyBoardVar.first = true
        }
        storyBoardVar.humanColor = color
        
        setBg()
        storyBoardVar.play = true
        storyBoardVar.turn = human
    }
    
    @IBAction func undo(_ sender: NSButton) {
        storyBoardVar.undo = true
    }
    
    
    func setBg(){
        let bg = Background();
        bg.createbg(screenSize:(scene.view?.bounds)!);
        scene.addChild(bg);
    }
    
    
    
}

