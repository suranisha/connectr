//
//  node.swift
//  connectR
//
//  Created by sha on 10/11/16.
//  Copyright © 2016 sha. All rights reserved.
//

import Foundation

struct Node{
    
    private var alpha:Int!
    private var beta:Int!
    private var value:Int!
    private var column:Int!
    private var win = false
    
    init(alpha:Int = -(Int)(UInt8.max), beta:Int = (Int)(UInt8.max), value:Int = -(Int)(UInt8.max), column:Int = -1) {
        self.alpha = alpha
        self.beta = beta
        self.value = value
        self.column = column
    }
    
    mutating func setAlpha(alpha:Int){
        self.alpha = alpha
    }
    
    mutating func setBeta(beta:Int){
        self.beta = beta
    }
    
    mutating func setValue(value:Int){
        self.value = value
    }
    
    mutating func setColumn(column:Int){
        self.column = column
    }
    
    mutating func setWin(win:Bool){
        self.win = win
    }
    
    
    func getAlpha()-> Int{
        return self.alpha
    }
    
    func getBeta()-> Int{
        return self.beta
    }
    
    func getValue()-> Int{
        return self.value
    }
    
    func getColumn()-> Int{
        return self.column
    }
    
    func getWin()-> Bool{
        return self.win
    }
    
    static func ==(l:Node, r:Node)->Bool{
        return l.alpha==r.alpha && l.beta==r.beta && l.value==r.value && l.win==r.win && l.column==r.column
    }
    
    
}
