//
//  connectR.swift
//  connectR
//
//  Created by sha on 10/10/16.
//  Copyright © 2016 sha. All rights reserved.
//

import Foundation
import SpriteKit

class connectR{
    
    var gameData = [[Int]]()    // 1:human; 0:computer played; -1:empty
    var size = [Int]()
    var lastPlayRow = [Int]()
    var lastPlayColumn = [Int]()
    
    
    init(Int n:Int, r:Int){
        gameData = Array(repeating: Array(repeating: -1, count: n), count: r)
        size = Array(repeating: 0, count: n)
        
    }
    
    func humanPlay(node:SKSpriteNode) -> SKSpriteNode {
        let column = Int(node.name!)!
        lastPlayColumn.append(column)
        lastPlayRow.append(size[column])
        
        
        if(!isFull(column: column)){
            gameData[size[column]][column] = 1
            size[column] += 1
            
            var disc:SKSpriteNode!
            if(storyBoardVar.humanColor==0){
                disc = SKSpriteNode(imageNamed: "red")
            }else{
                disc = SKSpriteNode(imageNamed: "blue")
            }
            
            return addBall(node: node, color: storyBoardVar.humanColor, disc: disc, name: "\(size[column]-1)-\(column)");
        }
        
        return SKSpriteNode()
    }
    
    func computerPlay(topMenu:SKSpriteNode) -> SKSpriteNode? {
        
        let column = getColumn();//get number from alpha beta pruning***********=================
        if(column<0){
            return SKSpriteNode()
        }
        print(column)
        lastPlayColumn.append(column)
        lastPlayRow.append(size[column])
        
        let node = topMenu.childNode(withName: "\(column)")
        if(!isFull(column: column)){
            gameData[size[column]][column] = 0
            size[column]+=1
            
            var disc:SKSpriteNode!
            if(storyBoardVar.humanColor==0){
                disc = SKSpriteNode(imageNamed: "blue")
            }else{
                disc = SKSpriteNode(imageNamed: "red")
            }
            
            return addBall(node: node as! SKSpriteNode, color: storyBoardVar.humanColor, disc: disc, name: "\(size[column]-1)-\(column)");
        }
        return SKSpriteNode()
    }
    
    func isFull(column:Int) -> Bool{
        if(column>storyBoardVar.column-1 || column < 0){
            return true
        }
        if(size[column] < gameData.count){
            return false
        }else{
            return true
        }
    }
    
    func addBall(node:SKSpriteNode, color:Int, disc:SKSpriteNode, name:String) -> SKSpriteNode{
        let disc:SKSpriteNode = disc
        disc.name = name
        disc.position = node.position
        disc.position.x+=node.size.width/2
        disc.xScale = node.xScale
        disc.yScale = node.yScale
        
        disc.zPosition = 1100
        
        let physicsBodySize = CGSize(width: node.size.width*0.9, height: node.size.height)
        disc.size = physicsBodySize
        
        
        
        
        disc.physicsBody = SKPhysicsBody(rectangleOf: physicsBodySize)
        disc.physicsBody?.categoryBitMask = 2
        disc.physicsBody?.collisionBitMask = 1 | UInt32(2)
        
        return disc
    }
    
    func undo() -> String? {
        if(lastPlayColumn.count==0){
            return nil
        }
        let column:Int = lastPlayColumn.popLast()!
        let row:Int = lastPlayRow.popLast()!
        let name:String = "\(row)-\(column)"
        gameData[row][column] = -1
        size[column]-=1
        
        return name
    }
    
    func getColumn()-> Int{
        let alphaBeta = AlphaBetaWithThread(data:gameData, size:self.size);
        let column = alphaBeta.start().getColumn()
        
        return column
        
    }
    
}
