//==============================================1===================================

////
////  AlphaBeta.swift
////  connectR
////
////  Created by sha on 10/11/16.
////  Copyright © 2016 sha. All rights reserved.
////
//
//import Foundation
//
//class AlphaBeta{
//    
//    let gameData:[[Int]]!
//    let size:[Int]!
//    
//    init(data:[[Int]], size:[Int]){
//        self.gameData = data
//        self.size = size
//    }
//    
//    func start() -> Int{
//        let column = alpha(data: self.gameData, size: self.size).getColumn()
//        if(column < 0){
//            print(column)
//            return getEmptyColumn()
//        }
//        return column
//    }
//    
//    
//    
//    func alpha(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
//        //depth
//        if(depth==0){
//            var valueNode = Node()
//            let win = didSomebodyWin(inColumn: column, inRow: size[column]-1, data: data)
//            valueNode.setValue(value: win.maxPoints)
//            valueNode.setColumn(column: column)
//            return valueNode
//        }
//        var newNode = node
//        for i in 0...storyBoardVar.column-1{
//            if(isColumnFull(column: i, size:size)){ continue }
//            //            if(!checkBound(column: size[i], row: i)) {continue}
//            
//            var newData = data
//            var newSize = size
//            
//            newData[size[i]][i] = 0
//            newSize[i] += 1
//            let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
//            if(win.win){
//                newNode.setValue(value: win.maxPoints)
//                newNode.setColumn(column: i)
//                return newNode
//            }
//            
//            //is it tie
//            if(isTie(size:newSize)){
//                newNode.setColumn(column: i)
//                newNode.setValue(value: win.maxPoints*2)
//                return newNode
//            }
//            
//            var minNode = beta(data: newData, size: newSize, depth: depth, column: i, node:newNode)
//            
//            if(depth-1 != 0){
//                minNode.setValue(value: win.maxPoints-minNode.getValue())
//            }
//            
//            if(minNode.getWin()){
//                newNode.setAlpha(alpha: -minNode.getValue())
//                newNode.setColumn(column: i)
//                return newNode
//            }else
//                if(minNode.getValue() > newNode.getAlpha()){
//                    newNode.setAlpha(alpha: minNode.getValue())
//                    newNode.setColumn(column: i)
//            }
//            //            if(node.getBeta() < newNode.getAlpha()){
//            //                return node
//            //            }
//        }
//        newNode.setValue(value: newNode.getAlpha())
//        return newNode
//    }
//    
//    //min
//    func beta(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
//        
//        var newNode = node
//        for i in 0...storyBoardVar.column-1{
//            //return if column is full
//            if(isColumnFull(column: i, size:size)){ continue }
//            //            if(!checkBound(column: size[i], row: i)) {continue}
//            
//            var newData = data
//            var newSize = size
//            newData[size[i]][i] = 1
//            newSize[i] += 1
//            let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
//            
//            if(win.win){
//                newNode.setValue(value: win.maxPoints*2)
//                newNode.setColumn(column: i)
//                return newNode
//            }
//            if(isTie(size:newSize)){
//                newNode.setValue(value: win.maxPoints)
//                newNode.setColumn(column: i)
//                return newNode
//            }
//            
//            var maxNode = alpha(data: newData, size: newSize, depth:depth-1, column: i, node: newNode)
//            
//            if(depth-1 != 0){
//                maxNode.setValue(value: -win.maxPoints+maxNode.getValue())
//            }
//            
//            
//            if(maxNode.getValue() < newNode.getBeta()){
//                newNode.setBeta(beta: maxNode.getValue())
//                newNode.setColumn(column: i)
//            }
//            
//            //            if(node.getAlpha()>newNode.getBeta()){
//            //                return node
//            //            }
//            
//        }
//        
//        newNode.setValue(value: -newNode.getBeta())
//        
//        return newNode
//        
//    }
//    
//    
//    
//    
//    
//    
//    //============================= helper functions =============================
//    
//    func points(column:Int, row:Int, data:[[Int]], size:[Int]) -> Node{
//        var node = Node()
//        let win = didSomebodyWin(inColumn: column, inRow: row, data: data)
//        if(win.player==0){
//            node.setValue(value: win.maxPoints)
//        }else if(win.player==1){
//            node.setValue(value: -win.maxPoints)
//        }
//        return node
//    }
//    
//    
//    
//    func isColumnFull(column:Int, size:[Int]) -> Bool{
//        return size[column] == storyBoardVar.row
//    }
//    
//    func didSomebodyWin(inColumn:Int, inRow:Int, data:[[Int]]) -> (win:Bool, player:Int, maxPoints:Int){
//        let discs = numberOfDiscInRow(inColumn: inColumn, inRow: inRow, data:data)
//        if(discs.numberOfDisc[0] == storyBoardVar.r || discs.numberOfDisc[1] == storyBoardVar.r || discs.numberOfDisc[2] == storyBoardVar.r || discs.numberOfDisc[3] == storyBoardVar.r ){
//            return (true, discs.player, getMax(myArray: discs.numberOfDisc))
//        }
//        return (false, discs.player, getMax(myArray: discs.numberOfDisc)) //-1 nobody win
//    }
//    
//    func isTie(size:[Int]) -> Bool {
//        var full:Bool = true
//        for i in 0...storyBoardVar.row-1{
//            if(size[i] != storyBoardVar.column){
//                full = false
//                break;
//            }
//        }
//        return full
//    }
//    
//    func numberOfDiscInRow(inColumn:Int, inRow:Int, data:[[Int]]) -> (player:Int, numberOfDisc:[Int]){
//        if(data[inRow][inColumn] == -1){
//            return (-1, [0,0,0,0])
//        }
//        let player = data[inRow][inColumn]
//        
//        var posibleOnTopRight = true
//        var posibleOnDownLeft = true
//        var numberOfDLDisc=1;
//        
//        var posibleOnRight = true;
//        var posibleOnLeft = true
//        var numberOfRLDisc=1;
//        
//        var posibleOnDownRight = true;
//        var posibleOnTopLeft = true
//        var numberOfDRDisc=1;
//        
//        var posibleOnDown = true;
//        var numberOfDDisc=1;
//        
//        for i in 1...storyBoardVar.r-1{
//            
//            //Horizontal check
//            
//            // top right
//            if(posibleOnTopRight && checkBound(column: inColumn+i, row: inRow+i)){
//                if(data[inRow+i][inColumn+i] == player){
//                    numberOfDLDisc+=1
//                }else{
//                    posibleOnTopRight = false;
//                }
//            }
//            
//            //right
//            if(posibleOnRight && checkBound(column: inColumn+i, row: inRow)){
//                if(data[inRow][inColumn+i] == player){
//                    numberOfRLDisc+=1
//                }else{
//                    posibleOnRight = false;
//                }
//            }
//            
//            // bottom right
//            if(posibleOnDownRight && checkBound(column: inColumn+i, row: inRow-i)){
//                if(data[inRow-i][inColumn+i] == player){
//                    numberOfDRDisc+=1
//                }else{
//                    posibleOnDownRight = false;
//                }
//            }
//            
//            // down
//            if(posibleOnDown && checkBound(column: inColumn, row: inRow-i)){
//                if(data[inRow-i][inColumn] == player){
//                    numberOfDDisc+=1
//                }else{
//                    posibleOnDown = false;
//                }
//            }
//            
//            // bottom left
//            if(posibleOnDownLeft && checkBound(column: inColumn-i, row: inRow-i)){
//                if(data[inRow-i][inColumn-i] == player){
//                    numberOfDLDisc+=1
//                }else{
//                    posibleOnDownLeft = false;
//                }
//            }
//            
//            //left
//            if(posibleOnLeft && checkBound(column: inColumn-i, row: inRow)){
//                if(data[inRow][inColumn-i] == player){
//                    numberOfRLDisc+=1
//                }else{
//                    posibleOnLeft = false;
//                }
//            }
//            
//            // top left
//            if(posibleOnTopLeft && checkBound(column: inColumn-i, row: inRow+i)){
//                if(data[inRow+i][inColumn-i] == player){
//                    numberOfDRDisc+=1
//                }else{
//                    posibleOnTopLeft = false;
//                }
//            }
//            
//            
//            
//            
//            
//            
//            if(posibleOnRight && posibleOnDown && posibleOnDownRight && posibleOnTopRight &&  posibleOnDownLeft && posibleOnLeft  &&  posibleOnTopLeft){
//                break ;
//            }
//        }
//        
//        return (player, [numberOfRLDisc, numberOfDRDisc, numberOfDDisc, numberOfDLDisc])
//    }
//    
//    func checkBound(column:Int, row:Int) -> Bool{
//        if(column<0 || row<0 || column>=storyBoardVar.column || row>=storyBoardVar.row){
//            return false
//        }
//        return true
//    }
//    
//    func getMax(myArray:[Int])-> Int{
//        var max = -storyBoardVar.r
//        for i in 0...myArray.count-1{
//            if(myArray[i]>max){
//                max = myArray[i]
//            }
//        }
//        return max
//    }
//    
//    func getMin(myArray:[Int]) -> Int {
//        var min = storyBoardVar.r
//        for i in 0...myArray.count-1{
//            if(myArray[i]<min){
//                min = -myArray[i]
//            }
//        }
//        return min
//    }
//    
//    func getEmptyColumn() -> Int {
//        var possibleColumn:[Int] = []
//        for i in 0...self.size.count-1{
//            if(!isColumnFull(column: i, size: self.size)){
//                possibleColumn.append(i)
//            }
//        }
//        if (possibleColumn.count>0){
//            return possibleColumn[Int( arc4random_uniform(UInt32((possibleColumn.count))))]
//        }else{
//            return -1
//        }
//    }
//}
//
//
//
//////return if somebody won
////let won = didSomebodyWin(inColumn:column, inRow:size[column])
////if won.win {
////    return node
////}
//
//
////func alphaBetaNode(column:Int) -> Node {
////
////    //return if column is full
////    if(isColumnFull(column: column)){ return node }
////
////    //depth arrive
////    if(self.depth==0){ return node }
////
////    //is it tie
////    if(isTie()){ return node }
////
////    //return if somebody won
////    let won = didSomebodyWin(inColumn:column, inRow:size[column])
////    if(won != -1){
////        return node
////    }
////
////    var newGameData = gameData
////    var newSize = self.size!
////    newSize[column] += 1
////
////    if(self.max == true) {
////        newGameData?[self.size[column]][column] = 0
////
////        let newAlphaBeta = AlphaBeta(data: newGameData!, size: newSize, depth:self.depth, max:false, node:Node(value:-storyBoardVar.r))  //recursive call
////        return newAlphaBeta.start()
////        //
////        //            let newNumberOfDiscInRow = newAlphaBeta.numberOfDiscInRow(inColumn: column, inRow: self.size[column])
////        //            let maxPossiblePoints:Int = getMax(myArray: newNumberOfDiscInRow.numberOfDisc)
////        //
////        //            self.node.setValue(value: maxPossiblePoints)
////        //            return self.node
////        //
////
////
////    }else{
////        //minimizer
////        newGameData?[self.size[column]][column] = 1
////
////        let newAlphaBeta = AlphaBeta(data: newGameData!, size: newSize, depth:self.depth-1, max:true)  //recursive call
////        newAlphaBeta.start()
////
////        let newNumberOfDiscInRow = newAlphaBeta.numberOfDiscInRow(inColumn: column, inRow: self.size[column])
////        let minPossiblePoints:Int = getMin(myArray: newNumberOfDiscInRow.numberOfDisc)
////
////        self.node.setValue(value: minPossiblePoints)
////        return self.node
////    }
////}
//
//
//
////=========================
////func alpha(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
////    //depth
////    if(depth==0){
////        var valueNode = Node()
////        let win = didSomebodyWin(inColumn: column, inRow: size[column]-1, data: data)
////        valueNode.setValue(value: -win.maxPoints)
////        valueNode.setColumn(column: column)
////        return valueNode
////    }
////    var newNode = node
////    for i in 0...storyBoardVar.column-1{
////        if(isColumnFull(column: i, size:size)){ continue }
////        //            if(!checkBound(column: size[i], row: i)) {continue}
////
////        var newData = data
////        var newSize = size
////
////        newData[size[i]][i] = 0
////        newSize[i] += 1
////        let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
////        if(win.win){
////            newNode.setAlpha(alpha: storyBoardVar.r)
////            newNode.setValue(value: storyBoardVar.r)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////
////        //is it tie
////        if(isTie(size:newSize)){
////            newNode.setColumn(column: i)
////            newNode.setValue(value: win.maxPoints)
////            return newNode
////        }
////
////
////        var minNode = beta(data: newData, size: newSize, depth: depth, column: i, node:newNode)
////
////        //            if(depth-1 != 0){
////        //                minNode.setValue(value: minNode.getValue()+win.maxPoints)
////        //            }else{
////        //                minNode.setValue(value: minNode.getValue())
////        //            }
////
////        if(minNode.getValue() > newNode.getAlpha()){
////            newNode.setAlpha(alpha: minNode.getValue())
////            newNode.setColumn(column: minNode.getColumn())
////        }
////        //            if(newNode.getAlpha() > newNode.getBeta()){
////        //                newNode.setValue(value: newNode.getAlpha())
////        //                return newNode;
////        //            }
////    }
////    newNode.setValue(value: newNode.getAlpha())
////    return newNode
////}
////
//////min
////func beta(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
////
////    var newNode = node
////    for i in 0...storyBoardVar.column-1{
////        //return if column is full
////        if(isColumnFull(column: i, size:size)){ continue }
////        if(!checkBound(column: size[i], row: i)) {continue}
////
////        var newData = data
////        var newSize = size
////        newData[size[i]][i] = 1
////        newSize[i] += 1
////        let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
////
////        //is it tie
////        if(isTie(size:newSize)){
////            newNode.setValue(value: -win.maxPoints)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////        if(win.win){
////            newNode.setValue(value: -win.maxPoints)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////
////        var maxNode = alpha(data: newData, size: newSize, depth:depth-1, column: i, node: newNode)
////
////        if(depth-1 != 0){
////            maxNode.setValue(value: maxNode.getValue()+win.maxPoints)
////        }else{
////            maxNode.setValue(value: maxNode.getValue())
////        }
////
////
////        if(maxNode.getValue() < newNode.getBeta()){
////            newNode.setBeta(beta: maxNode.getValue())
////            newNode.setColumn(column: maxNode.getColumn())
////        }
////
////        //            if(newNode.getValue() < newNode.getAlpha()){
////        //                newNode.setValue(value: newNode.getBeta())
////        //                return newNode
////        //            }
////        
////    }
////    
////    newNode.setValue(value: newNode.getBeta())
////    
////    return newNode
////    
////}
//
//

















//===============================================2===========================================
//
//  AlphaBeta.swift
//  connectR
//
//  Created by sha on 10/11/16.
//  Copyright © 2016 sha. All rights reserved.
//

//import Foundation
//
//class AlphaBeta{
//    
//    let gameData:[[Int]]!
//    let size:[Int]!
//    
//    init(data:[[Int]], size:[Int]){
//        self.gameData = data
//        self.size = size
//    }
//    
//    func start() -> Int{
//        let column = alpha(data: self.gameData, size: self.size).getColumn()
//        if(column < 0){
//            print(column)
//            return getEmptyColumn()
//        }
//        return column
//    }
//    
//    
//    
//    func alpha(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
//        //depth
//        if(depth==0){
//            var newNode = node
//            let point = points(data: data, size:size)
//            if(point.humanMaxPoints>point.computerMaxPoints && point.humanMaxPoints<=storyBoardVar.r){
//                newNode.setValue(value: -point.humanMaxPoints)
//                newNode.setColumn(column: column)
//                return newNode
//            }else if(point.computerMaxPoints>point.humanMaxPoints && point.computerMaxPoints<=storyBoardVar.r){
//                newNode.setValue(value: point.computerMaxPoints)
//                newNode.setColumn(column: column)
//                return newNode
//            }else{
//                //                print("both player have same points")
//                newNode.setValue(value: 0)
//                return newNode
//            }
//            
//        }
//        
//        var newNode = node
//        
//        for i in 0...storyBoardVar.column-1{
//            if(isColumnFull(column: i, size:size)){ continue }
//            
//            var newData = data
//            var newSize = size
//            
//            newData[size[i]][i] = 0
//            newSize[i] += 1
//            
//            //            if(depth==storyBoardVar.depth){
//            let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
//            if(win.win){
//                newNode.setColumn(column: i)
//                newNode.setValue(value: win.maxPoints)
//                return newNode
//            }
//            //            }
//            
//            let minNode = beta(data: newData, size: newSize, depth: depth, column: i, node:newNode)
//            
//            if(newNode.getAlpha()>minNode.getBeta()){
//                return newNode
//            }
//            
//            if(minNode.getValue() > newNode.getAlpha() && minNode.getWin()==false){
//                newNode.setAlpha(alpha: minNode.getValue())
//                newNode.setColumn(column: i)
//                newNode.setValue(value: newNode.getAlpha())
//            }
//        }
//        
//        return newNode
//    }
//    
//    //min
//    func beta(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
//        
//        var newNode = node
//        
//        for i in 0...storyBoardVar.column-1{
//            //return if column is full
//            if(isColumnFull(column: i, size:size)){ continue }
//            
//            var newData = data
//            var newSize = size
//            newData[size[i]][i] = 1
//            newSize[i] += 1
//            
//            let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
//            
//            if(win.win || newNode.getWin()){
//                if(newNode.getWin() && win.win){
//                    newNode.setBeta(beta: newNode.getBeta()+newNode.getBeta())
//                    newNode.setValue(value: newNode.getValue()+newNode.getValue())
//                }else if(win.win){
//                    newNode.setBeta(beta: -win.maxPoints)
//                    newNode.setValue(value: -win.maxPoints)
//                    newNode.setColumn(column: i)
//                    newNode.setWin(win: true)
//                }
//                continue
//            }
//            
//            let maxNode = alpha(data: newData, size: newSize, depth: depth-1, column: i, node:newNode)
//            if(newNode.getAlpha()>maxNode.getValue()){
//                return newNode
//            }
//            if(maxNode.getValue() < newNode.getBeta()){
//                newNode.setBeta(beta: maxNode.getValue())
//                newNode.setValue(value: maxNode.getValue())
//                newNode.setColumn(column: i)
//                if(maxNode.getWin()){
//                    newNode.setWin(win: maxNode.getWin())
//                }
//            }
//        }
//        
//        
//        
//        return newNode
//        
//    }
//    
//    
//    
//    
//    
//    
//    //============================= helper functions =============================
//    
//    func points(data:[[Int]], size:[Int]) -> (humanMaxPoints:Int, computerMaxPoints:Int){
//        var playerMaxPoints = 0
//        var computerMaxPoints = 0
//        
//        for i in 0...data.count-1 {
//            if(size[i] == 0){ continue }
//            for j in 0...size[i]-1 {
//                let win = didSomebodyWin(inColumn: i, inRow: j, data: data)
//                if(win.player==0){
//                    if(computerMaxPoints<win.maxPoints){
//                        computerMaxPoints =  win.maxPoints
//                    }
//                }else if(win.player==1){
//                    if(playerMaxPoints < win.maxPoints){
//                        playerMaxPoints = win.maxPoints
//                    }
//                }
//            }
//        }
//        
//        
//        return (playerMaxPoints, computerMaxPoints)
//    }
//    
//    
//    
//    func isColumnFull(column:Int, size:[Int]) -> Bool{
//        return size[column] == storyBoardVar.row
//    }
//    
//    func didSomebodyWin(inColumn:Int, inRow:Int, data:[[Int]]) -> (win:Bool, player:Int, maxPoints:Int){
//        let discs = numberOfDiscInRow(inColumn: inColumn, inRow: inRow, data:data)
//        if(discs.numberOfDisc[0] == storyBoardVar.r || discs.numberOfDisc[1] == storyBoardVar.r || discs.numberOfDisc[2] == storyBoardVar.r || discs.numberOfDisc[3] == storyBoardVar.r ){
//            return (true, discs.player, getMax(myArray: discs.numberOfDisc))
//        }
//        return (false, discs.player, getMax(myArray: discs.numberOfDisc)) //-1 nobody win
//    }
//    
//    func isTie(size:[Int]) -> Bool {
//        var full:Bool = true
//        for i in 0...storyBoardVar.row-1{
//            if(size[i] != storyBoardVar.column){
//                full = false
//                break;
//            }
//        }
//        return full
//    }
//    
//    func numberOfDiscInRow(inColumn:Int, inRow:Int, data:[[Int]]) -> (player:Int, numberOfDisc:[Int]){
//        if(data[inRow][inColumn] == -1){
//            return (-1, [0,0,0,0])
//        }
//        let player = data[inRow][inColumn]
//        
//        var posibleOnTopRight = true
//        var posibleOnDownLeft = true
//        var numberOfDLDisc=1;
//        
//        var posibleOnRight = true;
//        var posibleOnLeft = true
//        var numberOfRLDisc=1;
//        
//        var posibleOnDownRight = true;
//        var posibleOnTopLeft = true
//        var numberOfDRDisc=1;
//        
//        var posibleOnDown = true;
//        var numberOfDDisc=1;
//        
//        for i in 1...storyBoardVar.r-1{
//            
//            //Horizontal check
//            
//            // top right
//            if(posibleOnTopRight && checkBound(column: inColumn+i, row: inRow+i)){
//                if(data[inRow+i][inColumn+i] == player){
//                    numberOfDLDisc+=1
//                }else{
//                    posibleOnTopRight = false;
//                }
//            }
//            
//            //right
//            if(posibleOnRight && checkBound(column: inColumn+i, row: inRow)){
//                if(data[inRow][inColumn+i] == player){
//                    numberOfRLDisc+=1
//                }else{
//                    posibleOnRight = false;
//                }
//            }
//            
//            // bottom right
//            if(posibleOnDownRight && checkBound(column: inColumn+i, row: inRow-i)){
//                if(data[inRow-i][inColumn+i] == player){
//                    numberOfDRDisc+=1
//                }else{
//                    posibleOnDownRight = false;
//                }
//            }
//            
//            // down
//            if(posibleOnDown && checkBound(column: inColumn, row: inRow-i)){
//                if(data[inRow-i][inColumn] == player){
//                    numberOfDDisc+=1
//                }else{
//                    posibleOnDown = false;
//                }
//            }
//            
//            // bottom left
//            if(posibleOnDownLeft && checkBound(column: inColumn-i, row: inRow-i)){
//                if(data[inRow-i][inColumn-i] == player){
//                    numberOfDLDisc+=1
//                }else{
//                    posibleOnDownLeft = false;
//                }
//            }
//            
//            //left
//            if(posibleOnLeft && checkBound(column: inColumn-i, row: inRow)){
//                if(data[inRow][inColumn-i] == player){
//                    numberOfRLDisc+=1
//                }else{
//                    posibleOnLeft = false;
//                }
//            }
//            
//            // top left
//            if(posibleOnTopLeft && checkBound(column: inColumn-i, row: inRow+i)){
//                if(data[inRow+i][inColumn-i] == player){
//                    numberOfDRDisc+=1
//                }else{
//                    posibleOnTopLeft = false;
//                }
//            }
//            
//            
//            
//            
//            
//            
//            if(posibleOnRight && posibleOnDown && posibleOnDownRight && posibleOnTopRight &&  posibleOnDownLeft && posibleOnLeft  &&  posibleOnTopLeft){
//                break ;
//            }
//        }
//        
//        return (player, [numberOfRLDisc, numberOfDRDisc, numberOfDDisc, numberOfDLDisc])
//    }
//    
//    func checkBound(column:Int, row:Int) -> Bool{
//        if(column<0 || row<0 || column>=storyBoardVar.column || row>=storyBoardVar.row){
//            return false
//        }
//        return true
//    }
//    
//    func getMax(myArray:[Int])-> Int{
//        var max = -storyBoardVar.r
//        for i in 0...myArray.count-1{
//            if(myArray[i]>max){
//                max = myArray[i]
//            }
//        }
//        return max
//    }
//    
//    func getMin(myArray:[Int]) -> Int {
//        var min = storyBoardVar.r
//        for i in 0...myArray.count-1{
//            if(myArray[i]<min){
//                min = -myArray[i]
//            }
//        }
//        return min
//    }
//    
//    func getEmptyColumn() -> Int {
//        var possibleColumn:[Int] = []
//        for i in 0...self.size.count-1{
//            if(!isColumnFull(column: i, size: self.size)){
//                possibleColumn.append(i)
//            }
//        }
//        if (possibleColumn.count>0){
//            return possibleColumn[Int( arc4random_uniform(UInt32((possibleColumn.count))))]
//        }else{
//            return -1
//        }
//    }
//}
//
//
//
//////return if somebody won
////let won = didSomebodyWin(inColumn:column, inRow:size[column])
////if won.win {
////    return node
////}
//
//
////func alphaBetaNode(column:Int) -> Node {
////
////    //return if column is full
////    if(isColumnFull(column: column)){ return node }
////
////    //depth arrive
////    if(self.depth==0){ return node }
////
////    //is it tie
////    if(isTie()){ return node }
////
////    //return if somebody won
////    let won = didSomebodyWin(inColumn:column, inRow:size[column])
////    if(won != -1){
////        return node
////    }
////
////    var newGameData = gameData
////    var newSize = self.size!
////    newSize[column] += 1
////
////    if(self.max == true) {
////        newGameData?[self.size[column]][column] = 0
////
////        let newAlphaBeta = AlphaBeta(data: newGameData!, size: newSize, depth:self.depth, max:false, node:Node(value:-storyBoardVar.r))  //recursive call
////        return newAlphaBeta.start()
////        //
////        //            let newNumberOfDiscInRow = newAlphaBeta.numberOfDiscInRow(inColumn: column, inRow: self.size[column])
////        //            let maxPossiblePoints:Int = getMax(myArray: newNumberOfDiscInRow.numberOfDisc)
////        //
////        //            self.node.setValue(value: maxPossiblePoints)
////        //            return self.node
////        //
////
////
////    }else{
////        //minimizer
////        newGameData?[self.size[column]][column] = 1
////
////        let newAlphaBeta = AlphaBeta(data: newGameData!, size: newSize, depth:self.depth-1, max:true)  //recursive call
////        newAlphaBeta.start()
////
////        let newNumberOfDiscInRow = newAlphaBeta.numberOfDiscInRow(inColumn: column, inRow: self.size[column])
////        let minPossiblePoints:Int = getMin(myArray: newNumberOfDiscInRow.numberOfDisc)
////
////        self.node.setValue(value: minPossiblePoints)
////        return self.node
////    }
////}
//
//
//
////=========================
////func alpha(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
////    //depth
////    if(depth==0){
////        var valueNode = Node()
////        let win = didSomebodyWin(inColumn: column, inRow: size[column]-1, data: data)
////        valueNode.setValue(value: -win.maxPoints)
////        valueNode.setColumn(column: column)
////        return valueNode
////    }
////    var newNode = node
////    for i in 0...storyBoardVar.column-1{
////        if(isColumnFull(column: i, size:size)){ continue }
////        //            if(!checkBound(column: size[i], row: i)) {continue}
////
////        var newData = data
////        var newSize = size
////
////        newData[size[i]][i] = 0
////        newSize[i] += 1
////        let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
////        if(win.win){
////            newNode.setAlpha(alpha: storyBoardVar.r)
////            newNode.setValue(value: storyBoardVar.r)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////
////        //is it tie
////        if(isTie(size:newSize)){
////            newNode.setColumn(column: i)
////            newNode.setValue(value: win.maxPoints)
////            return newNode
////        }
////
////
////        var minNode = beta(data: newData, size: newSize, depth: depth, column: i, node:newNode)
////
////        //            if(depth-1 != 0){
////        //                minNode.setValue(value: minNode.getValue()+win.maxPoints)
////        //            }else{
////        //                minNode.setValue(value: minNode.getValue())
////        //            }
////
////        if(minNode.getValue() > newNode.getAlpha()){
////            newNode.setAlpha(alpha: minNode.getValue())
////            newNode.setColumn(column: minNode.getColumn())
////        }
////        //            if(newNode.getAlpha() > newNode.getBeta()){
////        //                newNode.setValue(value: newNode.getAlpha())
////        //                return newNode;
////        //            }
////    }
////    newNode.setValue(value: newNode.getAlpha())
////    return newNode
////}
////
//////min
////func beta(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
////
////    var newNode = node
////    for i in 0...storyBoardVar.column-1{
////        //return if column is full
////        if(isColumnFull(column: i, size:size)){ continue }
////        if(!checkBound(column: size[i], row: i)) {continue}
////
////        var newData = data
////        var newSize = size
////        newData[size[i]][i] = 1
////        newSize[i] += 1
////        let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
////
////        //is it tie
////        if(isTie(size:newSize)){
////            newNode.setValue(value: -win.maxPoints)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////        if(win.win){
////            newNode.setValue(value: -win.maxPoints)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////
////        var maxNode = alpha(data: newData, size: newSize, depth:depth-1, column: i, node: newNode)
////
////        if(depth-1 != 0){
////            maxNode.setValue(value: maxNode.getValue()+win.maxPoints)
////        }else{
////            maxNode.setValue(value: maxNode.getValue())
////        }
////
////
////        if(maxNode.getValue() < newNode.getBeta()){
////            newNode.setBeta(beta: maxNode.getValue())
////            newNode.setColumn(column: maxNode.getColumn())
////        }
////
////        //            if(newNode.getValue() < newNode.getAlpha()){
////        //                newNode.setValue(value: newNode.getBeta())
////        //                return newNode
////        //            }
////
////    }
////
////    newNode.setValue(value: newNode.getBeta())
////    
////    return newNode
////    
////}
//
//

//
//
//
////=========================================
////
////  AlphaBeta.swift
////  connectR
////
////  Created by sha on 10/11/16.
////  Copyright © 2016 sha. All rights reserved.
////
//
//import Foundation
//
//class AlphaBeta{
//    
//    let gameData:[[Int]]!
//    let size:[Int]!
//    var totalPoints = 0
//    
//    init(data:[[Int]], size:[Int]){
//        self.gameData = data
//        self.size = size
//    }
//    
//    func start() -> Int{
//        let column = alpha(data: self.gameData, size: self.size).getColumn()
//        print(totalPoints)
//        if(column < 0){
//            print(column)
//            return getEmptyColumn()
//        }
//        return column
//    }
//    
//    
//    
//    func alpha(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
//        
//        totalPoints += 1
//        //depth
//        if(depth==0){
//            var newNode = node
//            let point = points(data: data, size:size)
//            if(point.humanMaxPoints>point.computerMaxPoints && point.humanMaxPoints<=storyBoardVar.r){
//                newNode.setValue(value: -(point.humanMaxPoints*point.humanMaxPoints))
//                newNode.setColumn(column: column)
//                return newNode
//            }else if(point.computerMaxPoints>point.humanMaxPoints && point.computerMaxPoints<=storyBoardVar.r){
//                newNode.setValue(value: (point.computerMaxPoints*point.computerMaxPoints))
//                newNode.setColumn(column: column)
//                return newNode
//            }else{
//                newNode.setValue(value: 0)
//                return newNode
//            }
//            
//        }
//        
//        var newNode = node
//        
//        for i in 0...storyBoardVar.column-1{
//            if(isColumnFull(column: i, size:size)){ continue }
//            
//            var newData = data
//            var newSize = size
//            
//            newData[size[i]][i] = 0
//            newSize[i] += 1
//            
//            let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
//            if(win.win){
//                newNode.setColumn(column: i)
//                newNode.setValue(value: (win.maxPoints*win.maxPoints))
//                newNode.setWin(win: true)
//                return newNode
//            }
//            
//            let minNode = beta(data: newData, size: newSize, depth: depth, column: i, node:newNode)
//            
//            if(minNode.getWin()){
//                continue
//            }
//            if(newNode.getAlpha()>minNode.getBeta()){
//                return newNode
//            }
//            
//            if(minNode.getValue() > newNode.getAlpha()){
//                newNode.setAlpha(alpha: minNode.getValue())
//                newNode.setColumn(column: i)
//                newNode.setValue(value: newNode.getAlpha())
//            }
//        }
//        
//        return newNode
//        
//        
//    }
//    
//    //min
//    func beta(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
//        totalPoints += 1
//        var newNode = node
//        
//        for i in 0...storyBoardVar.column-1{
//            //return if column is full
//            if(isColumnFull(column: i, size:size)){ continue }
//            
//            var newData = data
//            var newSize = size
//            newData[size[i]][i] = 1
//            newSize[i] += 1
//            
//            let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
//            
//            if(win.win){
//                newNode.setColumn(column: i)
//                newNode.setValue(value: -(win.maxPoints*win.maxPoints))
//                newNode.setWin(win: true)
//                return newNode
//            }
//            
//            let maxNode = alpha(data: newData, size: newSize, depth: depth-1, column: i, node:newNode)
//            if(maxNode.getWin() && depth==storyBoardVar.depth){
//                continue
//            }
//            
//            if(newNode.getAlpha()>maxNode.getValue()){
//                return newNode
//            }
//            
//            if(maxNode.getValue()<newNode.getBeta()){
//                newNode.setBeta(beta: maxNode.getValue())
//                newNode.setValue(value: maxNode.getValue())
//                newNode.setColumn(column: i)
//            }
//        }
//        
//        
//        
//        return newNode
//        
//    }
//    
//    
//    
//    
//    
//    
//    //============================= helper functions =============================
//    
//    func points(data:[[Int]], size:[Int]) -> (humanMaxPoints:Int, computerMaxPoints:Int){
//        var playerMaxPoints = 0
//        var computerMaxPoints = 0
//        
//        for i in 0...data.count-1 {
//            if(size[i] == 0){ continue }
//            for j in 0...size[i]-1 {
//                let win = didSomebodyWin(inColumn: i, inRow: j, data: data)
//                if(win.player==0){
//                    if(computerMaxPoints<win.maxPoints){
//                        computerMaxPoints =  win.maxPoints
//                    }
//                }else if(win.player==1){
//                    if(playerMaxPoints < win.maxPoints){
//                        playerMaxPoints = win.maxPoints
//                    }
//                }
//            }
//        }
//        
//        
//        return (playerMaxPoints, computerMaxPoints)
//    }
//    
//    
//    
//    func isColumnFull(column:Int, size:[Int]) -> Bool{
//        return size[column] == storyBoardVar.row
//    }
//    
//    func didSomebodyWin(inColumn:Int, inRow:Int, data:[[Int]]) -> (win:Bool, player:Int, maxPoints:Int){
//        let discs = numberOfDiscInRow(inColumn: inColumn, inRow: inRow, data:data)
//        if(discs.numberOfDisc[0] == storyBoardVar.r || discs.numberOfDisc[1] == storyBoardVar.r || discs.numberOfDisc[2] == storyBoardVar.r || discs.numberOfDisc[3] == storyBoardVar.r ){
//            return (true, discs.player, getMax(myArray: discs.numberOfDisc))
//        }
//        return (false, discs.player, getMax(myArray: discs.numberOfDisc)) //-1 nobody win
//    }
//    
//    func isTie(size:[Int]) -> Bool {
//        var full:Bool = true
//        for i in 0...storyBoardVar.row-1{
//            if(size[i] != storyBoardVar.column){
//                full = false
//                break;
//            }
//        }
//        return full
//    }
//    
//    func numberOfDiscInRow(inColumn:Int, inRow:Int, data:[[Int]]) -> (player:Int, numberOfDisc:[Int]){
//        if(data[inRow][inColumn] == -1){
//            return (-1, [0,0,0,0])
//        }
//        let player = data[inRow][inColumn]
//        
//        var posibleOnTopRight = true
//        var posibleOnDownLeft = true
//        var numberOfDLDisc=1;
//        
//        var posibleOnRight = true;
//        var posibleOnLeft = true
//        var numberOfRLDisc=1;
//        
//        var posibleOnDownRight = true;
//        var posibleOnTopLeft = true
//        var numberOfDRDisc=1;
//        
//        var posibleOnDown = true;
//        var numberOfDDisc=1;
//        
//        for i in 1...storyBoardVar.r-1{
//            
//            //Horizontal check
//            
//            // top right
//            if(posibleOnTopRight && checkBound(column: inColumn+i, row: inRow+i)){
//                if(data[inRow+i][inColumn+i] == player){
//                    numberOfDLDisc+=1
//                }else{
//                    posibleOnTopRight = false;
//                }
//            }
//            
//            //right
//            if(posibleOnRight && checkBound(column: inColumn+i, row: inRow)){
//                if(data[inRow][inColumn+i] == player){
//                    numberOfRLDisc+=1
//                }else{
//                    posibleOnRight = false;
//                }
//            }
//            
//            // bottom right
//            if(posibleOnDownRight && checkBound(column: inColumn+i, row: inRow-i)){
//                if(data[inRow-i][inColumn+i] == player){
//                    numberOfDRDisc+=1
//                }else{
//                    posibleOnDownRight = false;
//                }
//            }
//            
//            // down
//            if(posibleOnDown && checkBound(column: inColumn, row: inRow-i)){
//                if(data[inRow-i][inColumn] == player){
//                    numberOfDDisc+=1
//                }else{
//                    posibleOnDown = false;
//                }
//            }
//            
//            // bottom left
//            if(posibleOnDownLeft && checkBound(column: inColumn-i, row: inRow-i)){
//                if(data[inRow-i][inColumn-i] == player){
//                    numberOfDLDisc+=1
//                }else{
//                    posibleOnDownLeft = false;
//                }
//            }
//            
//            //left
//            if(posibleOnLeft && checkBound(column: inColumn-i, row: inRow)){
//                if(data[inRow][inColumn-i] == player){
//                    numberOfRLDisc+=1
//                }else{
//                    posibleOnLeft = false;
//                }
//            }
//            
//            // top left
//            if(posibleOnTopLeft && checkBound(column: inColumn-i, row: inRow+i)){
//                if(data[inRow+i][inColumn-i] == player){
//                    numberOfDRDisc+=1
//                }else{
//                    posibleOnTopLeft = false;
//                }
//            }
//            
//            
//            
//            
//            
//            
//            if(posibleOnRight && posibleOnDown && posibleOnDownRight && posibleOnTopRight &&  posibleOnDownLeft && posibleOnLeft  &&  posibleOnTopLeft){
//                break ;
//            }
//        }
//        
//        return (player, [numberOfRLDisc, numberOfDRDisc, numberOfDDisc, numberOfDLDisc])
//    }
//    
//    func checkBound(column:Int, row:Int) -> Bool{
//        if(column<0 || row<0 || column>=storyBoardVar.column || row>=storyBoardVar.row){
//            return false
//        }
//        return true
//    }
//    
//    func getMax(myArray:[Int])-> Int{
//        var max = -storyBoardVar.r
//        for i in 0...myArray.count-1{
//            if(myArray[i]>max){
//                max = myArray[i]
//            }
//        }
//        return max
//    }
//    
//    func getMin(myArray:[Int]) -> Int {
//        var min = storyBoardVar.r
//        for i in 0...myArray.count-1{
//            if(myArray[i]<min){
//                min = -myArray[i]
//            }
//        }
//        return min
//    }
//    
//    func getEmptyColumn() -> Int {
//        var possibleColumn:[Int] = []
//        for i in 0...self.size.count-1{
//            if(!isColumnFull(column: i, size: self.size)){
//                possibleColumn.append(i)
//            }
//        }
//        if (possibleColumn.count>0){
//            return possibleColumn[Int( arc4random_uniform(UInt32((possibleColumn.count))))]
//        }else{
//            return -1
//        }
//    }
//}
//
//
//
//////return if somebody won
////let won = didSomebodyWin(inColumn:column, inRow:size[column])
////if won.win {
////    return node
////}
//
//
////func alphaBetaNode(column:Int) -> Node {
////
////    //return if column is full
////    if(isColumnFull(column: column)){ return node }
////
////    //depth arrive
////    if(self.depth==0){ return node }
////
////    //is it tie
////    if(isTie()){ return node }
////
////    //return if somebody won
////    let won = didSomebodyWin(inColumn:column, inRow:size[column])
////    if(won != -1){
////        return node
////    }
////
////    var newGameData = gameData
////    var newSize = self.size!
////    newSize[column] += 1
////
////    if(self.max == true) {
////        newGameData?[self.size[column]][column] = 0
////
////        let newAlphaBeta = AlphaBeta(data: newGameData!, size: newSize, depth:self.depth, max:false, node:Node(value:-storyBoardVar.r))  //recursive call
////        return newAlphaBeta.start()
////        //
////        //            let newNumberOfDiscInRow = newAlphaBeta.numberOfDiscInRow(inColumn: column, inRow: self.size[column])
////        //            let maxPossiblePoints:Int = getMax(myArray: newNumberOfDiscInRow.numberOfDisc)
////        //
////        //            self.node.setValue(value: maxPossiblePoints)
////        //            return self.node
////        //
////
////
////    }else{
////        //minimizer
////        newGameData?[self.size[column]][column] = 1
////
////        let newAlphaBeta = AlphaBeta(data: newGameData!, size: newSize, depth:self.depth-1, max:true)  //recursive call
////        newAlphaBeta.start()
////
////        let newNumberOfDiscInRow = newAlphaBeta.numberOfDiscInRow(inColumn: column, inRow: self.size[column])
////        let minPossiblePoints:Int = getMin(myArray: newNumberOfDiscInRow.numberOfDisc)
////
////        self.node.setValue(value: minPossiblePoints)
////        return self.node
////    }
////}
//
//
//
////=========================
////func alpha(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
////    //depth
////    if(depth==0){
////        var valueNode = Node()
////        let win = didSomebodyWin(inColumn: column, inRow: size[column]-1, data: data)
////        valueNode.setValue(value: -win.maxPoints)
////        valueNode.setColumn(column: column)
////        return valueNode
////    }
////    var newNode = node
////    for i in 0...storyBoardVar.column-1{
////        if(isColumnFull(column: i, size:size)){ continue }
////        //            if(!checkBound(column: size[i], row: i)) {continue}
////
////        var newData = data
////        var newSize = size
////
////        newData[size[i]][i] = 0
////        newSize[i] += 1
////        let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
////        if(win.win){
////            newNode.setAlpha(alpha: storyBoardVar.r)
////            newNode.setValue(value: storyBoardVar.r)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////
////        //is it tie
////        if(isTie(size:newSize)){
////            newNode.setColumn(column: i)
////            newNode.setValue(value: win.maxPoints)
////            return newNode
////        }
////
////
////        var minNode = beta(data: newData, size: newSize, depth: depth, column: i, node:newNode)
////
////        //            if(depth-1 != 0){
////        //                minNode.setValue(value: minNode.getValue()+win.maxPoints)
////        //            }else{
////        //                minNode.setValue(value: minNode.getValue())
////        //            }
////
////        if(minNode.getValue() > newNode.getAlpha()){
////            newNode.setAlpha(alpha: minNode.getValue())
////            newNode.setColumn(column: minNode.getColumn())
////        }
////        //            if(newNode.getAlpha() > newNode.getBeta()){
////        //                newNode.setValue(value: newNode.getAlpha())
////        //                return newNode;
////        //            }
////    }
////    newNode.setValue(value: newNode.getAlpha())
////    return newNode
////}
////
//////min
////func beta(data:[[Int]], size:[Int], depth:Int = storyBoardVar.depth, column:Int=0, node:Node = Node()) -> Node{
////
////    var newNode = node
////    for i in 0...storyBoardVar.column-1{
////        //return if column is full
////        if(isColumnFull(column: i, size:size)){ continue }
////        if(!checkBound(column: size[i], row: i)) {continue}
////
////        var newData = data
////        var newSize = size
////        newData[size[i]][i] = 1
////        newSize[i] += 1
////        let win = didSomebodyWin(inColumn: i, inRow: newSize[i]-1, data: newData)
////
////        //is it tie
////        if(isTie(size:newSize)){
////            newNode.setValue(value: -win.maxPoints)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////        if(win.win){
////            newNode.setValue(value: -win.maxPoints)
////            newNode.setColumn(column: i)
////            return newNode
////        }
////
////        var maxNode = alpha(data: newData, size: newSize, depth:depth-1, column: i, node: newNode)
////
////        if(depth-1 != 0){
////            maxNode.setValue(value: maxNode.getValue()+win.maxPoints)
////        }else{
////            maxNode.setValue(value: maxNode.getValue())
////        }
////
////
////        if(maxNode.getValue() < newNode.getBeta()){
////            newNode.setBeta(beta: maxNode.getValue())
////            newNode.setColumn(column: maxNode.getColumn())
////        }
////
////        //            if(newNode.getValue() < newNode.getAlpha()){
////        //                newNode.setValue(value: newNode.getBeta())
////        //                return newNode
////        //            }
////
////    }
////
////    newNode.setValue(value: newNode.getBeta())
////
////    return newNode
////
////}
//
//

